# API REST con NODEJS y JSONPlaceholder

## Requerimientos
Es necesario tener instalado nodejs para poder ejecutar el proyecto.
Puedes descargar nodejs en la [pagina oficial de nodejs](https://nodejs.org/es/).

Iniciamos nuestro proyecto

```bash
$ npm init
```

Completamos las preguntas de acuerdo a la informacion de nuestro proyecto o damos ENTER para dejar con los valores por defecto

## Instalacion

```bash
$ npm install --save json-server
```

Creamos el archivo db.json

```json
{
  "users": [
    { "id": 1, "firstname": "John", "lastname": "Doe", "email" : "johndoe@example.com", "age" : 31, "companyId" : "1" },
    { "id": 1, "firstname": "Mark", "lastname": "Wall", "email" : "markwall@example.com", "age" : 34, "companyId" : "2" },
    { "id": 1, "firstname": "Red", "lastname": "Doe", "email" : "reddoe@example.com", "age" : 27, "companyId" : "3" }
  ],
  "companies": [
    { "id": 1, "name": "Apple", "description": "Apple Inc. is an American multinational technology company headquartered in Cupertino, California that designs, develops, and sells consumer electronics, computer software, and online services." },
    { "id": 2, "name": "Microsoft", "description": "Microsoft Corporation is an American multinational technology company with headquarters in Redmond, Washington. It develops, manufactures, licenses, supports and sells computer software, consumer electronics, personal computers, and services."},
    { "id": 3, "name": "Google", "description": "Google is a multinational corporation that is specialized in Internet-related services and products."}
  ]
}

```

En scripts del archivo package.json agregamos lo siguiente

```json
{
  "scripts":{ 
    "json:server": "json-server --watch db.json" 
   }
}
```

En la linea de comandos ejecutamos

```bash
  $ npm run json:server
```

## Documentacion
Para poder acceder a la documentacion de la API accedemos a http://localhost:3000

## Rutas
Basandonos en el archivo `db.json` aqui se encuentran las rutas por defecto

### Rutas

```
GET    /users
GET    /users/1
POST   /users
PUT    /users/1
PATCH  /users/1
DELETE /users/1

GET    /companies
GET    /companies/1
POST   /companies
PUT    /companies/1
PATCH  /companies/1
DELETE /companies/1
```

### Filtros

Usar `.` para acceder a propiedades del objeto

```
GET /companies?name=Apple
GET /users?id=1&id=2
```

### Paginacion

Usar `_page` y opcionalmente `_limit` para paginar los datos devueltos.

En el `Enlace` del encabezado vas a disponer de los enlaces `first`, `prev`, `next` y `last`.


```
GET /users?_page=7
GET /users?_page=7&_limit=20
```

_10 items son devueltos por defecto_

### Ordenacion

Agregar `_sort` y `_order` (orden ascendente por defecto)

```
GET /users?_sort=lastname&_order=asc
```

### Busqueda completa

Agregar `q`

```
GET /users?q=ark
```

### Base de datos

```
GET /db
```

### Pagina de inicio

Retorna la pagina de inicio por defecto

```
GET /
```

## Extras

### Puerto alternativo

Puedes iniciar tu servidor JSON en otro puerto con la opcion `--port`:

```bash
$ json-server --watch db.json --port 3004
```

### Remote schema

Puedes cargar un schema remoto en vez de tener un archivo db.json local.

```bash
$ json-server http://example.com/file.json
$ json-server http://jsonplaceholder.typicode.com/db
```

Podemos agregar un nuevo servidor a nuestro package.json

```json
{
  "scripts": { 
    "json:server": "json-server --watch db.json",
    "json:server:remote": "json-server http://jsonplaceholder.typicode.com/db"
   }
}
```

Utilizamos el servidor remoto ejecutando en la consola

```bash
  $ npm run json:server:remote
```

### Postman Collection
Postman collection para consumir el API.
Importamos el archivo REST-API-JSONPlaceholder.postman_collection.json a nuestro Postman.

## Links
* [JSONPlaceholder](http://jsonplaceholder.typicode.com) es una API online falsa con tecnología de servidor JSON y ejecutándose en Heroku.
* [Postman](https://www.getpostman.com/) es una cadena de herramientas completa para desarrolladores de API.